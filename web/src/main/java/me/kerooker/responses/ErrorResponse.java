package me.kerooker.responses;

public class ErrorResponse {
    private String key;
    private String value;

    ErrorResponse(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public ErrorResponse() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
