package me.kerooker.model.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;

@JsonPropertyOrder({"name", "floor", "maxParticipants"})
public class Room {

    public static final Room EMPTY_ROOM = new Room();

    private String floor;
    private String name;
    private int maxParticipants;

    public Room(String floor, String name, int maxParticipants) {
        this.floor = floor;
        this.name = name;
        this.maxParticipants = maxParticipants;
    }

    public Room() {
    }

    @JsonProperty("floor")
    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(value = "maxParticipants")
    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
