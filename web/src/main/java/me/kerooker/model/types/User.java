package me.kerooker.model.types;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.Gson;

@JsonPropertyOrder({"name", "gender", "rg", "cpf"})
public class User {

    public static final User EMPTY_USER = new User();

    private String name;
    private String gender;
    private String cpf;
    private String rg;


    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(value = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isGenderEqual(String gender) {
        return gender.equalsIgnoreCase(getGender());
    }

    @JsonProperty(value = "cpf")
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @JsonProperty(value = "rg")
    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
