package me.kerooker.forms.room;

import com.google.gson.Gson;
import me.kerooker.model.types.Room;

public class CreateRoomForm {


    private String name;
    private String floor;
    private int maxParticipants;

    public CreateRoomForm(String name, String floor, int maxParticipants) {
        this.name = name;
        this.floor = floor;
        this.maxParticipants = maxParticipants;
    }

    public CreateRoomForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public Room toRoom() {
        Room r = new Room();
        r.setFloor(getFloor());
        r.setName(getName());
        r.setMaxParticipants(getMaxParticipants());
        return r;

    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
