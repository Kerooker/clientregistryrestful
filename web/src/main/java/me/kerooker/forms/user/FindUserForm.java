package me.kerooker.forms.user;

import com.google.gson.Gson;

public class FindUserForm {

    private String name;
    private int page;

    public FindUserForm(String name, int page) {
        this.name = name;
        this.page = page;
    }

    public FindUserForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
