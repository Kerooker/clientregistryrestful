package me.kerooker.forms.user;

import com.google.gson.Gson;

public class FindUserPageAmountForm {

    private String name;

    public FindUserPageAmountForm(String name) {
        this.name = name;
    }

    public FindUserPageAmountForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
