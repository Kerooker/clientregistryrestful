package me.kerooker.forms.user;

import me.kerooker.model.types.User;

public class CreateUserForm {

    private String name;
    private String gender;
    private String rg;
    private String cpf;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public User toUser() {
        User user = new User();
        user.setName(getName());
        user.setGender(getGender());
        user.setCpf(getCpf());
        user.setRg(getRg());

        return user;
    }
}
