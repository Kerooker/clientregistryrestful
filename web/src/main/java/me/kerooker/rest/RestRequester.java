package me.kerooker.rest;

import me.kerooker.handlers.UserHttpErrorHandler;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class RestRequester {

    private static <G> ResponseEntity<G> restfulGetRequest(String url, ParameterizedTypeReference<G> reference, String... params) {
        RestTemplate template = new RestTemplate();
        template.setErrorHandler(new UserHttpErrorHandler());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        return template.exchange(url, HttpMethod.GET, requestEntity, reference, params);
    }

    private static <G> ResponseEntity<G> restfulPostRequest(String url, ParameterizedTypeReference<G> reference, String jsonData) {

        RestTemplate template = new RestTemplate();
        template.setErrorHandler(new UserHttpErrorHandler());


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(jsonData, headers);
        return template.exchange(url, HttpMethod.POST, entity, reference);
    }


    public static <T> ResponseEntity<T> restfulGetSingleRequest(String url, ParameterizedTypeReference<T> reference, String... params) {
        return restfulGetRequest(url, reference, params);

    }

    public static <T> ResponseEntity<T> restfulPostSingleRequest(String url, ParameterizedTypeReference<T> reference, String jsonData) {
        return restfulPostRequest(url, reference, jsonData);
    }


}
