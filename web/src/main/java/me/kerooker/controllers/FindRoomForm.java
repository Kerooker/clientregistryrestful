package me.kerooker.controllers;

public class FindRoomForm {

    private String name;

    public FindRoomForm(String name) {
        this.name = name;
    }

    public FindRoomForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
