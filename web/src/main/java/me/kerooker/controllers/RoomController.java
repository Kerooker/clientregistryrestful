package me.kerooker.controllers;

import com.google.gson.Gson;
import me.kerooker.forms.room.CreateRoomForm;
import me.kerooker.model.types.Room;
import me.kerooker.rest.RestRequester;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/sala")
public class RoomController {

    @GetMapping("/registrar")
    public String registerRoom(Model model) {
        model.addAttribute("room", Room.EMPTY_ROOM);
        return "room/register_room";
    }

    @PostMapping("/registrar")
    @ResponseBody
    public String completeRegister(@ModelAttribute CreateRoomForm form) {
        //TODO

        String apiUrl = "http://localhost:4002/rooms";
        ParameterizedTypeReference<String> postType = new ParameterizedTypeReference<String>() {
        };

        ResponseEntity<String> response = RestRequester.restfulPostSingleRequest(apiUrl, postType, form.toJson());
        return response.getBody();
    }

    @GetMapping("/visualizar")
    public String viewRoom(Model model) {
        return "room/view_room";
    }

    @SuppressWarnings("unchecked")
    @PostMapping("/visualizar")
    @ResponseBody
    public String searchForUser(@ModelAttribute FindRoomForm form) {
        String url = "http://localhost:4002/rooms?name={name}";

        ParameterizedTypeReference<Map<String, String>> type = new ParameterizedTypeReference<Map<String, String>>() {
        };


        ResponseEntity<Map<String, String>> apiResponse = RestRequester.restfulGetSingleRequest(
                url, type, form.getName());


        Map<String, String> responseList = apiResponse.getBody();
        RoomResponse resp = new RoomResponse(apiResponse.getStatusCodeValue(), responseList);
        return new Gson().toJson(resp);
    }

    private class RoomResponse {
        private int statusCode;
        private Map<String, String> obj;

        public RoomResponse(int statusCode, Map<String, String> obj) {
            this.statusCode = statusCode;
            this.obj = obj;
        }

        public RoomResponse() {
        }

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public Map<String, String> getObj() {
            return obj;
        }

        public void setObj(Map<String, String> obj) {
            this.obj = obj;
        }
    }
}
