package me.kerooker.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import me.kerooker.forms.user.CreateUserForm;
import me.kerooker.forms.user.FindUserForm;
import me.kerooker.forms.user.FindUserPageAmountForm;
import me.kerooker.model.types.User;
import me.kerooker.responses.ErrorResponse;
import me.kerooker.rest.RestRequester;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/usuario")
public class UserController {

    private static final Type STRING_STRING_MAP_TYPE = new TypeToken<List<Map<String, String>>>() {
    }.getType();


    @GetMapping("/paginas")
    @ResponseBody
    public String getPageAmount(@ModelAttribute FindUserPageAmountForm form) {
        String urlPages = "http://localhost:4002/users/pages?name={name}";

        ParameterizedTypeReference<List<Map<String, String>>> type = new ParameterizedTypeReference<List<Map<String, String>>>() {
        };


        ResponseEntity<List<Map<String, String>>> apiResponsePages = RestRequester.restfulGetSingleRequest(
                urlPages, type, form.getName());

        return new Gson().toJson(apiResponsePages);
    }

    @GetMapping("/registrar")
    public String registerUser(Model model) {
        model.addAttribute("user", User.EMPTY_USER);
        return "user/register_user";
    }

    @PostMapping("/registrar")
    @ResponseBody
    public String completeRegister(@ModelAttribute CreateUserForm form) {

        String apiUrl = "http://localhost:4002/users";

        ParameterizedTypeReference<List<ErrorResponse>> type = new ParameterizedTypeReference<List<ErrorResponse>>() {
        };

        ResponseEntity<List<ErrorResponse>> responseFromApi = RestRequester.restfulPostSingleRequest(
                apiUrl, type, form.toUser().toJson());

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("error", Boolean.FALSE);

        String message;
        List<ErrorResponse> errors;
        if (responseFromApi.getStatusCode().equals(HttpStatus.OK)) {
            message = "Usuário inserido com sucesso!";
            jsonObject.addProperty("message", message);
        } else {
            jsonObject.addProperty("error", Boolean.TRUE);
            errors = responseFromApi.getBody();
            JsonArray arr = new JsonArray();
            for (ErrorResponse er : errors) {
                JsonObject obj = new JsonObject();
                obj.addProperty("field", er.getKey());
                obj.addProperty("message", er.getValue());
                arr.add(obj);
            }

            jsonObject.add("message", arr);
        }


        return jsonObject.toString();
    }

    @GetMapping("/visualizar")
    public String viewUser(Model model) {
        return "user/view_user";
    }

    @SuppressWarnings("unchecked")
    @PostMapping("/visualizar")
    @ResponseBody
    public String searchForUser(@ModelAttribute FindUserForm form) {
        String url = "http://localhost:4002/users?name={name}&page={page}";

        ParameterizedTypeReference<List<Map<String, String>>> type = new ParameterizedTypeReference<List<Map<String, String>>>() {
        };


        ResponseEntity<List<Map<String, String>>> apiResponse = RestRequester.restfulGetSingleRequest(
                url, type, form.getName(), form.getPage() + "");


        List<Map<String, String>> responseList = apiResponse.getBody();
        return new Gson().toJson(orderByNamesInAlphabeticOrder(responseList));
    }

    private List<Map<String, String>> orderByNamesInAlphabeticOrder(List<Map<String, String>> order) {
        List<Map<String, String>> copy = new ArrayList<>(order);
        copy.sort(Comparator.comparing(o -> o.get("name")));
        return copy;
    }


}
