package me.kerooker.controllers;

import me.kerooker.exceptions.user.UserAlreadyExistException;
import me.kerooker.exceptions.user.UserNotFoundException;
import me.kerooker.exceptions.user.UserUpdateWithNoChangeException;
import me.kerooker.model.dto.types.User;
import me.kerooker.model.entities.UserEntity;
import me.kerooker.requests.user.UserPostRequest;
import me.kerooker.requests.user.UserPutRequest;
import me.kerooker.responses.PaginationResponse;
import me.kerooker.responses.UserResponse;
import me.kerooker.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/pages")
    public ResponseEntity<PaginationResponse> getPages(@RequestParam(value = "name") String name) {

        int pages = userService.getUserPageAmount(name);
        PaginationResponse response = new PaginationResponse(pages);

        return ResponseEntity.ok(response);
    }


    @GetMapping
    public ResponseEntity<List<UserResponse>> getUser(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "page") int page) {

        List<UserResponse> userResponseList = new ArrayList<>();
        List<UserEntity> ce = userService.listUserEntitiesByName(name, true, page);

        for (UserEntity userEntity : ce) {
            userResponseList.add(new UserResponse(User.from(userEntity)));
        }
        return ResponseEntity.ok(userResponseList);
    }

    @DeleteMapping("/{cpf}")
    public ResponseEntity<Void> deactivateUser(@PathVariable("cpf") String cpf) throws UserNotFoundException {

        UserEntity entity = userService.getUser(cpf);

        userService.deactivateUser(entity);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/{cpf}")
    public ResponseEntity<Void> updateUser(
            @PathVariable("cpf") String cpf,
            @Valid @RequestBody UserPutRequest userPutRequest) throws UserNotFoundException {

        UserEntity entity = userService.getUser(cpf);

        UserEntity newInformationUser = UserEntity.from(userPutRequest);

        try {
            userService.updateUser(entity, newInformationUser);
        } catch (UserUpdateWithNoChangeException e) {
            //Fail silently
        }

        return ResponseEntity.ok().build();

    }

    @PostMapping
    public ResponseEntity<Void> addUser(@Valid @RequestBody UserPostRequest userPostRequest) throws UserAlreadyExistException {

        UserEntity userEntity = UserEntity.from(userPostRequest);

        userService.create(userEntity);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
