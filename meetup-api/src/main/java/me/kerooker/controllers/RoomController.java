package me.kerooker.controllers;

import me.kerooker.exceptions.room.RoomAlreadyExistException;
import me.kerooker.exceptions.room.RoomNotFoundException;
import me.kerooker.exceptions.room.RoomUpdateWithNoChangeException;
import me.kerooker.model.entities.RoomEntity;
import me.kerooker.requests.room.RoomPostRequest;
import me.kerooker.requests.room.RoomPutRequest;
import me.kerooker.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/rooms", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomController {

    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService service) {
        this.roomService = service;
    }

    @DeleteMapping("/{roomName}")
    public ResponseEntity<Void> deactivateRoom(@PathVariable String roomName) throws RoomNotFoundException {

        roomService.deactivate(roomName);

        return ResponseEntity.ok().build();
    }


    @PostMapping
    public ResponseEntity<Void> createRoom(
            @Valid @RequestBody RoomPostRequest roomPostRequest) throws RoomAlreadyExistException {

        RoomEntity ent = RoomEntity.from(roomPostRequest);

        roomService.create(ent);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/{roomName}")
    public ResponseEntity<RoomEntity> updateRoom(
            @PathVariable String roomName,
            @Valid @RequestBody RoomPutRequest request) throws RoomUpdateWithNoChangeException, RoomNotFoundException {

        RoomEntity newInformationEntity = RoomEntity.from(request);

        RoomEntity updated = roomService.updateEntity(roomName, newInformationEntity);

        return ResponseEntity.ok(updated);

    }

    @GetMapping
    public ResponseEntity<RoomEntity> getRoom(@RequestParam("name") String roomName) throws RoomNotFoundException {
        RoomEntity room = roomService.getRoomEntity(roomName);
        return ResponseEntity.ok(room);

    }

}
