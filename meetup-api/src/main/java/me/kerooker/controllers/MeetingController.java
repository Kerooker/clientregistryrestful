package me.kerooker.controllers;


import me.kerooker.exceptions.meeting.MeetingException;
import me.kerooker.exceptions.room.RoomNotFoundException;
import me.kerooker.exceptions.user.UserNotFoundException;
import me.kerooker.model.dto.types.meeting.RecurrentMeeting;
import me.kerooker.model.entities.MeetingEntity;
import me.kerooker.model.entities.UserEntity;
import me.kerooker.requests.meeting.MeetingPostRequest;
import me.kerooker.responses.MeetingResponse;
import me.kerooker.services.MeetingService;
import me.kerooker.services.RoomService;
import me.kerooker.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestMapping("/meetings")
@RestController
public class MeetingController {

    private MeetingService meetingService;
    private RoomService roomService;
    private UserService userService;

    @Autowired
    public MeetingController(MeetingService meetingService, RoomService roomService, UserService userService) {
        this.meetingService = meetingService;
        this.roomService = roomService;
        this.userService = userService;
    }


    @GetMapping
    public ResponseEntity getMeetings(@RequestParam String userCpf) {
        UserEntity user;
        try {
            user = userService.getUser(userCpf);
        } catch (UserNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }


        List<MeetingEntity> organizing = meetingService.getMeetingOrganizedBy(user);

        List<MeetingEntity> futureMeetings = user.getFutureMeetings();
        List<MeetingEntity> pastMeetings = user.getPastMeetings();

        List<MeetingResponse> futureMeetingResponses = futureMeetings.stream()
                .map(MeetingResponse::new)
                .collect(Collectors.toList());


        List<MeetingResponse> pastMeetingResponses = pastMeetings.stream()
                .map(MeetingResponse::new)
                .collect(Collectors.toList());
        List<MeetingResponse> organizingMeetingsResponse = organizing.stream()
                .map(MeetingResponse::new)
                .collect(Collectors.toList());


        Map<String, List<MeetingResponse>> response = new HashMap<>();
        response.put("organizing", organizingMeetingsResponse);
        response.put("future", futureMeetingResponses);
        response.put("past", pastMeetingResponses);

        return ResponseEntity.ok(response);


    }

    @PostMapping
    public ResponseEntity bookMeeting(@Valid @RequestBody MeetingPostRequest request) {

        MeetingEntity meeting;
        try {
            meeting = request.toMeetingEntity(userService, roomService);
        } catch (UserNotFoundException | RoomNotFoundException | MeetingException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        try {
            if (request.isRecurring()) {
                meetingService.bookRecurringMeeting(RecurrentMeeting.from(request, userService, roomService));
            } else {
                meetingService.bookMeeting(meeting);
            }
        } catch (MeetingException | UserNotFoundException | RoomNotFoundException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }

        return ResponseEntity.ok().build();


    }

}
