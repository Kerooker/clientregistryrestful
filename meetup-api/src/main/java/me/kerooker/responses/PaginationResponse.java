package me.kerooker.responses;

public class PaginationResponse extends ValueResponse<Integer> {

    public PaginationResponse(Integer value) {
        super(value);
    }
}
