package me.kerooker.responses;

public class ValueResponse<T> {

    private T value;

    public ValueResponse(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
