package me.kerooker.exceptionhandlers;

import me.kerooker.exceptions.KerookerException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class KerookerExceptionHandler extends ResponseEntityExceptionHandler {


    @Override
    @ResponseBody
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<FieldError> errors = ex.getBindingResult().getFieldErrors();

        List<ErrorResponse> errorsFiltered = errors.stream()
                .map(ErrorResponse::new).collect(Collectors.toList());

        return ResponseEntity.badRequest().body(errorsFiltered);
    }

    class ErrorResponse {
        private String key;
        private String value;

        ErrorResponse(FieldError err) {
            this(err.getField(), err.getDefaultMessage());
        }

        ErrorResponse(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

    @ExceptionHandler(KerookerException.class)
    @ResponseBody
    protected ResponseEntity<ErrorResponse> handleKerookerException(KerookerException ex) {

        ErrorResponse err = new ErrorResponse("message", ex.getMessage());

        return ResponseEntity.badRequest().body(err);
    }

}
