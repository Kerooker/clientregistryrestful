package me.kerooker.requests.room;

public abstract class RoomRequest {

    protected String name;
    protected String floor;
    protected Integer maxParticipants;


    public abstract String getName();

    public void setName(String name) {
        this.name = name;
    }

    public abstract String getFloor();

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public abstract Integer getMaxParticipants();

    public void setMaxParticipants(Integer maxParticipants) {
        this.maxParticipants = maxParticipants;
    }
}
