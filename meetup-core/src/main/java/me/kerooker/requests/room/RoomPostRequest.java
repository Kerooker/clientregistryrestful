package me.kerooker.requests.room;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

public class RoomPostRequest extends RoomRequest {

    @NotBlank
    public String getName() {
        return name;
    }

    @NotBlank
    public String getFloor() {
        return floor;
    }

    @NotNull
    @Range(min = 2)
    public Integer getMaxParticipants() {
        return maxParticipants;
    }
}
