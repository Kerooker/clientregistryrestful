package me.kerooker.requests.room;

import me.kerooker.annotations.NullOrNotBlank;

public class RoomPutRequest extends RoomRequest {


    @NullOrNotBlank
    public String getName() {
        return name;
    }

    @NullOrNotBlank
    public String getFloor() {
        return floor;
    }

    @NullOrNotBlank
    public Integer getMaxParticipants() {
        return maxParticipants;
    }
}
