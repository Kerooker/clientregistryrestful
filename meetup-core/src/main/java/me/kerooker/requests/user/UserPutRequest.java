package me.kerooker.requests.user;

import com.google.gson.Gson;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Pattern;

import static me.kerooker.Utils.UserConstants.REAL_NAME_REGEX;
import static me.kerooker.Utils.UserConstants.RG_REGEX;

public class UserPutRequest extends UserRequest {

    @Pattern(regexp = REAL_NAME_REGEX)
    public String getName() {
        return name;
    }

    @CPF
    public String getCpf() {
        return cpf;
    }

    @Pattern(regexp = RG_REGEX)
    public String getRg() {
        return rg;
    }

    public String toJson() {

        return new Gson().toJson(this);
    }

}
