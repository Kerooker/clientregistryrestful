package me.kerooker.requests.user;

import me.kerooker.model.dto.types.Gender;

import java.util.Optional;

public abstract class UserRequest {


    protected String name;
    protected String cpf;
    protected String rg;
    protected Gender gender;


    public abstract String getName();

    public void setName(String name) {
        this.name = name;
    }

    public abstract String getCpf();

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public abstract String getRg();

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Optional<Gender> getGender() {
        return Optional.ofNullable(gender);
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
