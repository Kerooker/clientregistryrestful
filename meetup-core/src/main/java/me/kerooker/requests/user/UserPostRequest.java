package me.kerooker.requests.user;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static me.kerooker.Utils.UserConstants.REAL_NAME_REGEX;
import static me.kerooker.Utils.UserConstants.RG_REGEX;

public class UserPostRequest extends UserRequest {

    @NotBlank
    @Pattern(regexp = REAL_NAME_REGEX)
    public String getName() {
        return name;
    }

    @NotNull
    @CPF
    public String getCpf() {
        return cpf;
    }

    @Pattern(regexp = RG_REGEX)
    public String getRg() {
        return rg;
    }

}
