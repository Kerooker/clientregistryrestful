package me.kerooker.requests.meeting;

import me.kerooker.model.dto.types.meeting.RecurrenceType;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

public class MeetingPostRequest extends MeetingRequest {


    @NotNull
    public boolean isRecurring() {
        return recurring;
    }

    //Nullable
    public RecurrenceType getRecurrenceType() {
        return recurrenceType;
    }

    //Nullable
    public LocalDateTime getRecurrenceEndDate() {
        return recurrenceEndDate;
    }

    @NotNull
    public LocalDateTime getStartDate() {
        return startDate;
    }

    @NotNull
    public LocalDateTime getEndDate() {
        return endDate;
    }

    @CPF
    public String getOrganizingUserCpf() {
        return organizingUserCpf;
    }

    @Size(min = 1)
    public List<String> getParticipantUsersCpf() {
        return participantUsersCpf;
    }

    @NotBlank
    public String getRoom() {
        return room;
    }

    @SuppressWarnings("unused")
    @AssertTrue(message = "Booking a recurring meeting requires an end date")
    private boolean isRecurrenceCorrectlySetup() {

        return !recurring || (getRecurrenceEndDate() != null && getRecurrenceType() != null && getRecurrenceEndDate().isAfter(LocalDateTime.now()));
    }

    @SuppressWarnings("unused")
    @AssertTrue
    private boolean isValidDateRange() {
        LocalDateTime now = LocalDateTime.now();

        boolean startIsFuture = getStartDate().isAfter(now);
        boolean endIsAfterStart = getEndDate().isAfter(getStartDate());

        return startIsFuture && endIsAfterStart;
    }


}
