package me.kerooker.requests.meeting;

import me.kerooker.exceptions.meeting.MeetingException;
import me.kerooker.exceptions.room.RoomNotFoundException;
import me.kerooker.exceptions.user.UserNotFoundException;
import me.kerooker.model.dto.types.meeting.RecurrenceType;
import me.kerooker.model.entities.MeetingEntity;
import me.kerooker.model.entities.RoomEntity;
import me.kerooker.model.entities.UserEntity;
import me.kerooker.services.RoomService;
import me.kerooker.services.UserService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public abstract class MeetingRequest {

    protected LocalDateTime startDate;
    protected LocalDateTime endDate;
    protected String organizingUserCpf;
    protected List<String> participantUsersCpf;
    protected String room;
    protected boolean recurring;
    protected LocalDateTime recurrenceEndDate;
    protected RecurrenceType recurrenceType;


    public abstract RecurrenceType getRecurrenceType();

    public void setRecurrenceType(RecurrenceType recurrenceType) {
        this.recurrenceType = recurrenceType;
    }

    public abstract LocalDateTime getRecurrenceEndDate();

    public void setRecurrenceEndDate(LocalDateTime recurrenceEndDate) {
        this.recurrenceEndDate = recurrenceEndDate;
    }

    public abstract boolean isRecurring();


    public void setRecurring(boolean recurring) {
        this.recurring = recurring;
    }

    public abstract LocalDateTime getStartDate();

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public abstract LocalDateTime getEndDate();

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public abstract String getOrganizingUserCpf();

    public void setOrganizingUserCpf(String organizingUserCpf) {
        this.organizingUserCpf = organizingUserCpf;
    }

    public abstract List<String> getParticipantUsersCpf();

    public void setParticipantUsersCpf(List<String> participantUsersCpf) {
        this.participantUsersCpf = participantUsersCpf;
    }

    public abstract String getRoom();

    public void setRoom(String room) {
        this.room = room;
    }


    //FIXME
    public MeetingEntity toMeetingEntity(UserService userService, RoomService roomService) throws MeetingException, RoomNotFoundException, UserNotFoundException {
        MeetingEntity meeting = new MeetingEntity();

        meeting.setStartDate(getStartDate());
        meeting.setEndDate(getEndDate());

        UserEntity organizer = userService.getUser(getOrganizingUserCpf());
        meeting.setOrganizingUser(organizer);


        List<UserEntity> participatingUsers = new ArrayList<>();
        for (String c : getParticipantUsersCpf()) {
            String cpf = c;
            UserEntity participant = userService.getUser(cpf);

            participatingUsers.add(participant);
        }

        meeting.setParticipantUsers(participatingUsers);

        String room = getRoom();

        RoomEntity roomEntity = roomService.getRoomEntity(room);
        meeting.setRoom(roomEntity);

        return meeting;

    }
}
