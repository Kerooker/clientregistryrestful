package me.kerooker.annotations;

import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Validates if an is either null or a non-blank considered object.
 * <p>
 * Example:
 * <p>
 * <pre>{@code
 *    @literal @NullOrNotBlank
 *     String s; // S can be null or anything that's not blank
 *    @literal @NullOrNotBlank
 *     Integer i;   //i can be null or anything that's not blank
 *     }
 * </pre>
 *
 * @author lcolman
 */
@Pattern(regexp = "\\S+")
@ReportAsSingleViolation
@Documented
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
public @interface NullOrNotBlank {


}
