package me.kerooker.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import me.kerooker.model.entities.RoomEntity;

@JsonPropertyOrder({"name", "floor", "maxParticipants"})
public class RoomResponse {

    private RoomEntity room;

    public RoomResponse(RoomEntity room) {
        
        this.room = room;
    }

    @JsonProperty("name")
    public String getName() {
        return room.getName();
    }

    @JsonProperty("floor")
    public String getFloor() {
        return room.getFloor();
    }

    @JsonProperty("maxParticipants")
    public Integer getMaxParticipants() {
        return room.getMaxParticipants();
    }
}
