package me.kerooker.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import me.kerooker.model.entities.MeetingEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({"startdate", "enddate", "organizer", "participants", "room"})
public class MeetingResponse {

    private MeetingEntity entity;

    public MeetingResponse(MeetingEntity entity) {
        this.entity = entity;
    }


    @JsonProperty(value = "startdate")
    public LocalDateTime getStart() {
        return entity.getStartDate();
    }

    @JsonProperty(value = "enddate")
    public LocalDateTime getEnd() {
        return entity.getEndDate();
    }

    @JsonProperty(value = "organizer")
    public UserResponse getOrganizingUser() {
        return new UserResponse(entity.getOrganizingUser());
    }

    @JsonProperty(value = "room")
    public RoomResponse getRoom() {
        return new RoomResponse(entity.getRoom());
    }

    @JsonProperty(value = "participants")
    public List<UserResponse> getParticipatingUsers() {
        List<UserResponse> list = new ArrayList<>();
        entity.getParticipantUsers().forEach(u -> list.add(new UserResponse(u)));
        return list;
    }
}
