package me.kerooker.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import me.kerooker.model.dto.types.Gender;
import me.kerooker.model.dto.types.User;
import me.kerooker.model.entities.UserEntity;

@JsonPropertyOrder({"name", "cpf", "rg", "gender"})
public class UserResponse {

    private User user;

    public UserResponse(User user) {
        this.user = user;
    }

    public UserResponse(UserEntity entity) {
        this(User.from(entity));
    }


    @JsonProperty("name")
    public String getName() {
        return user.getName();
    }

    @JsonProperty("cpf")
    public String getCpf() {
        return user.getCpf();
    }

    @JsonProperty("rg")
    public String getRg() {
        return user.getRg();
    }

    @JsonProperty("gender")
    public Gender getGender() {
        return user.getGender();
    }
}
