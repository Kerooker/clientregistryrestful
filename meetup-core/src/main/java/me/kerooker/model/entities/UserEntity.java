package me.kerooker.model.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import me.kerooker.model.dto.types.Gender;
import me.kerooker.requests.user.UserRequest;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    private String rg;

    @Column(name = "cpf", unique = true, nullable = false)
    private String cpf;

    private boolean active = true;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @JsonIgnore
    @ManyToMany(mappedBy = "participantUsers")
    private List<MeetingEntity> meetings;

    public UserEntity() {

    }

    public UserEntity(UserEntity userEntity) {
        this();
        setId(userEntity.getId());
        setName(userEntity.getName());
        setCpf(userEntity.getCpf());
        setRg(userEntity.getRg());
        setActive(userEntity.isActive());
        setMeetings(userEntity.getMeetings());
        setGender(userEntity.getGender());
    }

    public static UserEntity from(UserRequest request) {
        UserEntity entity = new UserEntity();

        entity.setName(request.getName());
        entity.setRg(request.getRg());
        entity.setCpf(request.getCpf());
        entity.setGender(request.getGender().isPresent() ? request.getGender().get() : null);
        return entity;

    }

    @Override
    public String toString() {
        return toJson();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserEntity) {
            if (toJson().equals(((UserEntity) obj).toJson())) {
                return true;
            }
        }
        return false;
    }

    public List<MeetingEntity> getMeetings() {
        return meetings;
    }

    public void setMeetings(List<MeetingEntity> meetings) {
        this.meetings = meetings;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public UserEntity mergeWith(UserEntity entityWithNewInformation) {

        UserEntity newEntity = new UserEntity(this);

        String cpf = entityWithNewInformation.getCpf();
        String rg = entityWithNewInformation.getRg();
        String name = entityWithNewInformation.getName();
        Gender gender = entityWithNewInformation.getGender();

        if (cpf != null) newEntity.setCpf(cpf);
        if (rg != null) newEntity.setRg(rg);
        if (name != null) newEntity.setName(name);
        if (gender != Gender.UNDEFINED && gender != null) newEntity.setGender(gender);
        newEntity.setId(getId());

        return newEntity;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public List<MeetingEntity> getFutureMeetings() {
        return meetings.stream().filter(MeetingEntity::isFuture).collect(Collectors.toList());
    }

    public List<MeetingEntity> getPastMeetings() {
        return meetings.stream().filter(ent -> !ent.isFuture()).collect(Collectors.toList());
    }
}
