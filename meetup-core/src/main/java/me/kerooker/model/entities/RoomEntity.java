package me.kerooker.model.entities;

import com.google.gson.Gson;
import me.kerooker.requests.room.RoomRequest;

import javax.persistence.*;

@Entity
@Table(name = "room", uniqueConstraints = @UniqueConstraint(name = "IDX_NAME", columnNames = {"name"}))
public class RoomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(unique = true, nullable = false, name = "name")
    private String name;

    @Column(name = "floor", nullable = false)
    private String floor;

    @Column(nullable = false, name = "max_participants")
    private Integer maxParticipants = 2;

    @Column(name = "active")
    private boolean active = true;

    public static RoomEntity from(RoomRequest request) {
        RoomEntity entity = new RoomEntity();
        entity.setName(request.getName());
        entity.setFloor(request.getFloor());
        entity.setMaxParticipants(request.getMaxParticipants());
        return entity;

    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RoomEntity) {
            return toJson().equals(((RoomEntity) obj).toJson());
        }
        return false;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public Integer getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(Integer maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public RoomEntity mergeWith(RoomEntity mergeEntity) {
        String newName = mergeEntity.getName();
        String newFloor = mergeEntity.getFloor();
        Integer newMax = mergeEntity.getMaxParticipants();

        RoomEntity newEntity = new RoomEntity();

        if (newName != null) newEntity.setName(newName);
        if (newFloor != null) newEntity.setFloor(newFloor);
        if (newMax != null) newEntity.setMaxParticipants(newMax);
        newEntity.setId(getId());
        return newEntity;

    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
