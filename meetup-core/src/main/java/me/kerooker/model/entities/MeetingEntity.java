package me.kerooker.model.entities;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "meeting")
public class MeetingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @Column(name = "dat_start", nullable = false)
    private LocalDateTime startDate;

    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @Column(name = "dat_end", nullable = false)
    private LocalDateTime endDate;

    @OneToOne
    @JoinColumn(name = "organizing_user", referencedColumnName = "id")
    private UserEntity organizingUser;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_meeting")
    private List<UserEntity> participantUsers;

    @OneToOne
    private RoomEntity room;

    @Column(name = "active")
    private boolean active;

    @Column(name = "recurring")
    private boolean recurring;


    public boolean isRecurring() {
        return recurring;
    }

    public void setRecurring(boolean recurring) {
        this.recurring = recurring;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public UserEntity getOrganizingUser() {
        return organizingUser;
    }

    public void setOrganizingUser(UserEntity organizingUser) {
        this.organizingUser = organizingUser;
    }

    public List<UserEntity> getParticipantUsers() {
        return participantUsers;
    }

    public void setParticipantUsers(List<UserEntity> participantUsers) {
        this.participantUsers = participantUsers;
    }

    public RoomEntity getRoom() {
        return room;
    }

    public void setRoom(RoomEntity room) {
        this.room = room;
    }

    public boolean isFuture() {
        return getStartDate().isAfter(LocalDateTime.now());
    }
}
