package me.kerooker.model.dto.types.meeting;

import me.kerooker.exceptions.meeting.MeetingException;
import me.kerooker.exceptions.room.RoomNotFoundException;
import me.kerooker.exceptions.user.UserNotFoundException;
import me.kerooker.model.entities.MeetingEntity;
import me.kerooker.requests.meeting.MeetingRequest;
import me.kerooker.services.RoomService;
import me.kerooker.services.UserService;

import java.time.LocalDateTime;

public class RecurrentMeeting {

    private RecurrenceType recurrenceType;
    private MeetingEntity meetingEntity;
    private LocalDateTime recurrenceEndTime;

    public RecurrentMeeting(RecurrenceType recurrenceType, MeetingEntity meetingEntity, LocalDateTime recurrencyEndTime) {
        this.recurrenceType = recurrenceType;
        this.meetingEntity = meetingEntity;
        this.recurrenceEndTime = recurrencyEndTime;
    }

    public static RecurrentMeeting from(MeetingRequest request, UserService userService, RoomService roomService) throws UserNotFoundException, MeetingException, RoomNotFoundException {
        MeetingEntity meetingEntity = request.toMeetingEntity(userService, roomService);
        RecurrenceType recurrenceType = request.getRecurrenceType();
        LocalDateTime recurrenceEndDate = request.getRecurrenceEndDate();
        return new RecurrentMeeting(recurrenceType, meetingEntity, recurrenceEndDate);
    }


    public RecurrenceType getRecurrenceType() {
        return recurrenceType;
    }

    public void setRecurrenceType(RecurrenceType recurrenceType) {
        this.recurrenceType = recurrenceType;
    }

    public MeetingEntity getMeetingEntity() {
        return meetingEntity;
    }

    public void setMeetingEntity(MeetingEntity meetingEntity) {
        this.meetingEntity = meetingEntity;
    }

    public LocalDateTime getRecurrenceEndTime() {
        return recurrenceEndTime;
    }

    public void setRecurrenceEndTime(LocalDateTime recurrenceEndTime) {
        this.recurrenceEndTime = recurrenceEndTime;
    }
}
