package me.kerooker.model.dto.types;

import com.google.gson.Gson;
import me.kerooker.model.entities.UserEntity;
import me.kerooker.requests.user.UserRequest;

public class User {

    private final String name;
    private final String cpf;
    private final String rg;
    private final Gender gender;

    public User(String name, String cpf, String rg, Gender gender) {
        this.name = name;
        this.cpf = cpf;
        this.rg = rg;
        this.gender = gender == null ? Gender.UNDEFINED : gender;
    }


    public static User from(UserEntity entity) {
        return new User(
                entity.getName(),
                entity.getCpf(),
                entity.getRg(),
                entity.getGender());

    }

    public static User from(UserRequest req) {
        return new User(
                req.getName(),
                req.getCpf(),
                req.getRg(),
                req.getGender().isPresent() ? req.getGender().get() : null);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User) {
            if (((User) obj).toJson().equals(this.toJson())) {
                return true;
            }
        }
        return false;
    }

    public User createUserWithNewInformation(User newInformationUser) {

        String newName = getName();
        String newCpf = getCpf();
        String newRg = getRg();
        Gender newGender = getGender();

        if (newInformationUser.getName() != null) newName = newInformationUser.getName();
        if (newInformationUser.getCpf() != null) newCpf = newInformationUser.getCpf();
        if (newInformationUser.getRg() != null) newRg = newInformationUser.getRg();
        if (newInformationUser.getGender() != Gender.UNDEFINED) newGender = newInformationUser.getGender();

        return new User(newName, newCpf, newRg, newGender);

    }

    public String getName() {
        return name;
    }

    public String getCpf() {
        return cpf;
    }

    public String getRg() {
        return rg;
    }

    public Gender getGender() {
        return gender;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}
