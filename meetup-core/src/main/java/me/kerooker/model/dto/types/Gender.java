package me.kerooker.model.dto.types;

public enum Gender {

    MALE,
    FEMALE,
    UNDEFINED,;
}
