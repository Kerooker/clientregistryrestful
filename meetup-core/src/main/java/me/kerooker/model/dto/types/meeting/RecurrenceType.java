package me.kerooker.model.dto.types.meeting;

import java.time.temporal.ChronoUnit;

public enum RecurrenceType {

    DAILY(ChronoUnit.DAYS),
    WEEKLY(ChronoUnit.WEEKS),
    MONTHLY(ChronoUnit.MONTHS),;

    private ChronoUnit unitOfTime;

    RecurrenceType(ChronoUnit unitOfTime) {
        this.unitOfTime = unitOfTime;
    }

    public ChronoUnit getUnitOfTime() {
        return unitOfTime;
    }
}
