package me.kerooker.model.dto.types;

import me.kerooker.model.entities.RoomEntity;

public class Room {

    private String name;
    private String floor;
    private Integer maxParticipants;


    public static Room from(RoomEntity entity) {
        Room r = new Room();
        r.setName(entity.getName());
        r.setMaxParticipants(entity.getMaxParticipants());
        r.setFloor(entity.getFloor());
        return r;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public Integer getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(Integer maxParticipants) {
        this.maxParticipants = maxParticipants;
    }
}
