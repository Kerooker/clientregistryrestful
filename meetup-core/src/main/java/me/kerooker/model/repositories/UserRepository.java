package me.kerooker.model.repositories;

import me.kerooker.model.entities.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Integer> {


    UserEntity findByCpf(String cpf);

    Page<UserEntity> findByNameContains(Pageable pageable, String name);



}
