package me.kerooker.model.repositories;

import me.kerooker.model.entities.RoomEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends CrudRepository<RoomEntity, Integer> {

    RoomEntity findByNameEquals(String name);


}
