package me.kerooker.model.repositories;

import me.kerooker.model.entities.MeetingEntity;
import me.kerooker.model.entities.RoomEntity;
import me.kerooker.model.entities.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface MeetingRepository extends CrudRepository<MeetingEntity, Integer> {

    @Query(value = "select m from MeetingEntity m where m.room = ?1 and (m.startDate between ?2 and ?3 or m.endDate between ?2 and ?3)")
    MeetingEntity findByRoomAndStartDateAndEndDateNotBooked(RoomEntity room, LocalDateTime startDate, LocalDateTime endDate);

    List<MeetingEntity> findByOrganizingUser(UserEntity user);
}
