package me.kerooker.services;

import me.kerooker.exceptions.meeting.MeetingException;
import me.kerooker.exceptions.meeting.RecurrentMeetingBookingException;
import me.kerooker.model.dto.types.meeting.RecurrentMeeting;
import me.kerooker.model.entities.MeetingEntity;
import me.kerooker.model.entities.RoomEntity;
import me.kerooker.model.entities.UserEntity;
import me.kerooker.model.repositories.MeetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;


@Service
public class MeetingService {

    private MeetingRepository repository;


    @Autowired
    public MeetingService(MeetingRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void bookMeeting(MeetingEntity entity) throws MeetingException {
        bookRoomForDate(entity);
    }

    public List<MeetingEntity> getMeetingOrganizedBy(UserEntity user) {
        return repository.findByOrganizingUser(user);
    }

    @Transactional(rollbackFor = {RecurrentMeetingBookingException.class})
    public void bookRecurringMeeting(RecurrentMeeting meeting) throws RecurrentMeetingBookingException {
        LocalDateTime start = meeting.getMeetingEntity().getStartDate();
        LocalDateTime end = meeting.getMeetingEntity().getEndDate();

        while (start.isBefore(meeting.getRecurrenceEndTime())) {
            MeetingEntity recurrent = copyOfRecurrent(meeting, start, end);

            try {
                bookRoomForDate(recurrent);
            } catch (MeetingException e) {
                throw new RecurrentMeetingBookingException(e.getMessage());
            }
            start = start.plus(1, meeting.getRecurrenceType().getUnitOfTime());
            end = end.plus(1, meeting.getRecurrenceType().getUnitOfTime());
        }

    }

    private MeetingEntity copyOfRecurrent(RecurrentMeeting meeting, LocalDateTime start, LocalDateTime end) {
        MeetingEntity entity = new MeetingEntity();
        entity.setRecurring(true);
        entity.setActive(true);
        entity.setStartDate(start);
        entity.setEndDate(end);
        entity.setOrganizingUser(meeting.getMeetingEntity().getOrganizingUser());
        entity.setParticipantUsers(meeting.getMeetingEntity().getParticipantUsers());
        entity.setRoom(meeting.getMeetingEntity().getRoom());
        return entity;
    }


    private boolean isRoomAvailable(RoomEntity ent, LocalDateTime startTime, LocalDateTime endTime) {
        MeetingEntity s = repository.findByRoomAndStartDateAndEndDateNotBooked(ent, startTime, endTime);
        return (s == null);
    }

    private void bookRoomForDate(MeetingEntity entity) throws MeetingException {
        if (!isRoomAvailable(entity.getRoom(), entity.getStartDate(), entity.getEndDate())) {
            throw new MeetingException("Room isn't available");
        }

        save(entity);

    }

    private void save(MeetingEntity meeting) {
        repository.save(meeting);
    }
}
