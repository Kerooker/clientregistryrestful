package me.kerooker.services;

import me.kerooker.exceptions.room.RoomAlreadyExistException;
import me.kerooker.exceptions.room.RoomNotFoundException;
import me.kerooker.exceptions.room.RoomUpdateWithNoChangeException;
import me.kerooker.model.entities.RoomEntity;
import me.kerooker.model.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository repo) {
        this.roomRepository = repo;
    }


    @SuppressWarnings("ConstantConditions")
    @Transactional
    public void create(RoomEntity ent) throws RoomAlreadyExistException {

        Optional<RoomEntity> roomEntityOptional = getRoomEntityBy(ent.getName());

        if (!roomEntityOptional.isPresent()) {
            roomRepository.save(ent);
            return;
        }

        RoomEntity databaseEntity = roomEntityOptional.get();
        if (databaseEntity.isActive()) {
            throw new RoomAlreadyExistException("The room " + ent.getName() + " already exists and can't be created.");
        } else {
            databaseEntity.setActive(true);
            roomRepository.save(databaseEntity);
        }

    }


    public RoomEntity getRoomEntity(String name) throws RoomNotFoundException {
        Optional<RoomEntity> entityOptional = getRoomEntityBy(name);
        if (!entityOptional.isPresent()) {
            throw new RoomNotFoundException("Room " + name + " couldn't be found.");
        }
        return entityOptional.get();

    }

    @Transactional
    public RoomEntity updateEntity(String entityName, RoomEntity entityWithNewInformation) throws RoomNotFoundException, RoomUpdateWithNoChangeException {
        Optional<RoomEntity> entityOptional = getRoomEntityBy(entityName);
        if (!entityOptional.isPresent()) {
            throw new RoomNotFoundException("Room " + entityName + " couldn't be found. Impossible to update.");
        }

        RoomEntity currentEntity = entityOptional.get();

        RoomEntity mergedEntity = currentEntity.mergeWith(entityWithNewInformation);

        if (mergedEntity.equals(currentEntity)) {
            throw new RoomUpdateWithNoChangeException("No changes were made to room " + entityName + ". Impossible to update.");
        }

        roomRepository.save(mergedEntity);

        return mergedEntity;

    }

    @Transactional
    public void deactivate(String room) throws RoomNotFoundException {
        Optional<RoomEntity> entityOptional = getRoomEntityBy(room);
        if (!entityOptional.isPresent()) {
            throw new RoomNotFoundException("The room " + room + " wasn't found.");
        }

        deactivateFromDatabase(entityOptional.get());
    }

    private Optional<RoomEntity> getRoomEntityBy(String name) {
        return Optional.ofNullable(roomRepository.findByNameEquals(name));
    }

    private void deactivateFromDatabase(RoomEntity entity) {
        entity.setActive(false);
        roomRepository.save(entity);
    }
}
