package me.kerooker.services;

import me.kerooker.exceptions.user.UserAlreadyExistException;
import me.kerooker.exceptions.user.UserNotFoundException;
import me.kerooker.exceptions.user.UserUpdateWithNoChangeException;
import me.kerooker.model.entities.UserEntity;
import me.kerooker.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private static final int DEFAULT_PAGE_SIZE = 20;
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public List<UserEntity> listUserEntitiesByName(@NotNull String name, boolean ignoreDisabled, int page) {

        return userRepository.findByNameContains(new PageRequest(page, DEFAULT_PAGE_SIZE), name).getContent().stream()
                .filter(user -> (user.isActive() || !ignoreDisabled))
                .collect(Collectors.toList());
    }

    public int getUserPageAmount(@NotNull String name) {
        return userRepository.findByNameContains(new PageRequest(0, DEFAULT_PAGE_SIZE), name).getTotalPages();
    }

    @Transactional
    public void deactivateUser(UserEntity entity) {
        deactivateUserByCpf(entity.getCpf());
    }

    @Transactional
    public void create(UserEntity userEntity) throws UserAlreadyExistException {

        if (getUserByCpf(userEntity.getCpf()).isPresent()) {
            throw new UserAlreadyExistException("User with CPF" + userEntity.getCpf() + " already exists. Impossible to create");
        }

        userRepository.save(userEntity);
    }

    @Transactional
    public void updateUser(UserEntity entity, UserEntity entityWithNewInformation) throws UserUpdateWithNoChangeException {

        UserEntity merged = entity.mergeWith(entityWithNewInformation);
        if (merged.equals(entity)) {
            throw new UserUpdateWithNoChangeException("User " + entity.toString() + " wasn't updated.");
        }

        userRepository.save(merged);
    }

    public UserEntity getUser(@NotNull String cpf) throws UserNotFoundException {
        Optional<UserEntity> userEntityOptional = getUserByCpf(cpf);

        if (!userEntityOptional.isPresent()) {
            throw new UserNotFoundException("User of CPF " + cpf + " couldn't be found.");
        }

        return userEntityOptional.get();
    }

    private void deactivateUserByCpf(@NotNull String cpf) {
        Optional<UserEntity> entity = getUserByCpf(cpf);

        entity.ifPresent(e -> {
            e.setActive(false);
            userRepository.save(entity.get());
        });
    }

    private Optional<UserEntity> getUserByCpf(@NotNull String cpf) {
        return Optional.ofNullable(userRepository.findByCpf(cpf));
    }


}
