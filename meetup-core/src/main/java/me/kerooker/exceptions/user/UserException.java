package me.kerooker.exceptions.user;

import me.kerooker.exceptions.KerookerException;

public class UserException extends KerookerException {

    private String message;

    public UserException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
