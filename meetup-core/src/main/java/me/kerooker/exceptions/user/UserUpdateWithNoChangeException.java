package me.kerooker.exceptions.user;

public class UserUpdateWithNoChangeException extends UserException {
    public UserUpdateWithNoChangeException(String message) {
        super(message);
    }
}
