package me.kerooker.exceptions.room;

import me.kerooker.exceptions.KerookerException;

public class RoomException extends KerookerException {

    private String message;

    public RoomException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
