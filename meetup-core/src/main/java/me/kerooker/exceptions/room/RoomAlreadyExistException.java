package me.kerooker.exceptions.room;

public class RoomAlreadyExistException extends RoomException {

    public RoomAlreadyExistException(String message) {
        super(message);
    }
}
