package me.kerooker.exceptions.room;

public class RoomUpdateWithNoChangeException extends RoomException {

    public RoomUpdateWithNoChangeException(String message) {
        super(message);
    }
}
