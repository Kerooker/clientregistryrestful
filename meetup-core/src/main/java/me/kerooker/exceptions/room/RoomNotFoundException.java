package me.kerooker.exceptions.room;

public class RoomNotFoundException extends RoomException {

    public RoomNotFoundException(String message) {
        super(message);
    }
}
