package me.kerooker.exceptions.meeting;

public class RecurrentMeetingBookingException extends MeetingRecurrenceException {
    public RecurrentMeetingBookingException(String message) {
        super(message);
    }
}
