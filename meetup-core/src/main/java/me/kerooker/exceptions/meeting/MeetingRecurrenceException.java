package me.kerooker.exceptions.meeting;

public class MeetingRecurrenceException extends MeetingException {
    public MeetingRecurrenceException(String message) {
        super(message);
    }
}
