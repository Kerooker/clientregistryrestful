package me.kerooker.exceptions.meeting;

public class MeetingNotFoundException extends MeetingException {
    public MeetingNotFoundException(String message) {
        super(message);
    }
}
