package me.kerooker.exceptions.meeting;

import me.kerooker.exceptions.KerookerException;

public class MeetingException extends KerookerException {

    private String message;

    public MeetingException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
