package me.kerooker.Utils;

public class StringUtil {

    public static Integer[] convertIntegerStringToIntegerArray(String toConvert) {

        Integer[] converted = new Integer[toConvert.length()];

        for (int i = 0; i < toConvert.length(); i++) {
            converted[i] = Integer.parseInt(String.valueOf(toConvert.charAt(i)));
        }

        return converted;

    }

    public static String cleanString(String toClean, String[] charactersToClear) {
        for (String s : charactersToClear) {
            toClean = toClean.replaceAll(s, "");
        }
        return toClean;
    }


}
