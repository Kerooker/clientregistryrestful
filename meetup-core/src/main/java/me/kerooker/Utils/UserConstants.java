package me.kerooker.Utils;

public class UserConstants {

    public static final Integer CPF_MAX_SIZE = 11;

    public static final Integer RG_MAX_SIZE = 100;

    public static final Integer NAME_MAX_SIZE = 100;

    public static final String REAL_NAME_REGEX = "^[\\p{L} .'-]+$";
    public static final String RG_REGEX = "[0-9]+[0-9xX]";


}
