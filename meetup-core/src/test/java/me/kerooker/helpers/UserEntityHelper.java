package me.kerooker.helpers;

import me.kerooker.model.dto.types.Gender;
import me.kerooker.model.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class UserEntityHelper {

    public static UserEntity createUserEntity() {

        UserEntity userEntity = new UserEntity();
        userEntity.setActive(true);
        userEntity.setId(1);
        userEntity.setName("Leonardo Colman Lopes");
        userEntity.setRg("552455179");
        userEntity.setCpf("41462780822");
        userEntity.setGender(Gender.MALE);
        return userEntity;

    }

    public static List<UserEntity> createUserEntityList(int size, boolean active) {
        List<UserEntity> list = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            UserEntity e = createUserEntity();
            e.setActive(active);
            list.add(createUserEntity());
        }
        return list;
    }
}
