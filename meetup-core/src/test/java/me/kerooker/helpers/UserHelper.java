package me.kerooker.helpers;

import me.kerooker.model.dto.types.User;
import me.kerooker.model.entities.UserEntity;

public class UserHelper {

    public static User createUser(UserEntity entity) {
        return User.from(entity);
    }
}
