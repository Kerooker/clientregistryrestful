package me.kerooker.helpers;

import me.kerooker.model.entities.MeetingEntity;
import me.kerooker.model.entities.RoomEntity;
import me.kerooker.model.entities.UserEntity;

import java.time.LocalDateTime;
import java.util.Arrays;

public class MeetingEntityHelper {


    public static MeetingEntity createMeetingEntityWithFakeUsers(LocalDateTime startingDate, LocalDateTime endingDate) {
        UserEntity userEntity = UserEntityHelper.createUserEntity();
        RoomEntity roomEntity = RoomEntityHelper.createRoomEntity();

        MeetingEntity meetingEntity = new MeetingEntity();

        meetingEntity.setActive(true);
        meetingEntity.setId(1);
        meetingEntity.setStartDate(startingDate);
        meetingEntity.setEndDate(endingDate);
        meetingEntity.setOrganizingUser(userEntity);
        meetingEntity.setParticipantUsers(Arrays.asList(userEntity, userEntity, userEntity));
        meetingEntity.setRoom(roomEntity);

        return meetingEntity;


    }


}
