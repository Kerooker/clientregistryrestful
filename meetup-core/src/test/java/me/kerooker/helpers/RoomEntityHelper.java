package me.kerooker.helpers;

import me.kerooker.model.entities.RoomEntity;

public class RoomEntityHelper {


    public static RoomEntity createRoomEntity() {

        RoomEntity ent = new RoomEntity();
        ent.setActive(true);
        ent.setId(1);
        ent.setName("Room");
        ent.setFloor("Floor");
        ent.setMaxParticipants(42);

        return ent;


    }


}
